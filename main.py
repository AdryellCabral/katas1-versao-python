#O conteúdo desta lista de exercícios é funções, estruturas de repetição, 
#declaração de varaiáveis, métodos de lista e raciocínio lógico matemático.

def one_through_twenty():
    sequencia = []
    for i in range(1, 21):
        sequencia.append(i)
    return sequencia

#Retornar os números de 1 a 20. (1, 2, 3,…, 19, 20)

def evens_to_twenty():
    sequencia_pares = []
    for i in range(2, 21, 2):
        sequencia_pares.append(i)  
    return sequencia_pares

#Retornar os números pares de 1 a 20. (2, 4, 6,…, 18, 20)

#Eu poderia verificar o resto de uma divisão pra determinar se é ímpar ou par, 
#poŕem não usaria 100% do recurso estudado, no caso, a propriedade STEP do range.

def odds_to_twenty():
    sequencia_impares = []
    for i in range(1, 21, 2):    
        sequencia_impares.append(i)
    return sequencia_impares

#Retornar os números ímpares de 1 a 20. (1, 3, 5,…, 17, 19)

def multiples_of_five():
    multiplos_de_cinco = []
    for i in range(5, 101, 5):
        multiplos_de_cinco.append(i)
    return multiplos_de_cinco

#Retornar os múltiplos de 5 até 100. (5, 10, 15,…, 95, 100)

def square_numbers():
    quadrados_perfeitos = []
    for i in range(1, 101):
        numero = int(i ** (1/2))
        if ((numero ** 2) == i):
            quadrados_perfeitos.append(i)
    return quadrados_perfeitos

#Retornar todos os números até 100 que forem quadrados perfeitos. (1, 4, 9, …, 81, 100)

def counting_backwards():
    sequencia_inversa = []
    for i in range(20, 0, -1):
        sequencia_inversa.append(i)
    return sequencia_inversa

#Retornar os números contando de trás para frente de 20 até 1. (20, 19, 18, …, 2, 1)

def even_numbers_backwards():
    pares_inversos = []
    for i in range(20, 0, -2):
        pares_inversos.append(i)
    return pares_inversos

#Retornar os números pares de 20 até 1. (20, 18, 16, …, 4, 2)

def odd_numbers_backwards():
    impares_inversos = []
    for i in range(19, 0, -2):
        impares_inversos.append(i)
    return impares_inversos

#Retornar os números ímpares de 20 até 1. (19, 17, 15, …, 3, 1)

def multiple_of_five_backwards():
    multiplos_de_cinco_inversos = []
    for i in range(100, 4, -5):
        multiplos_de_cinco_inversos.append(i)
    return multiplos_de_cinco_inversos

#Retornar os múltiplos de 5 contando de trás para frente a partir de 100. (100, 95, 90, …, 10, 5)

def square_numbers_backwards():
    quadrados_perfeitos_inversos = []
    for i in range(100, 0, -1):
        numero = int(i ** (1/2))
        if ((numero ** 2) == i):
            quadrados_perfeitos_inversos.append(i)
    return quadrados_perfeitos_inversos

#Retornar os quadrados perfeitos contando de trás para frente a partir de 100. (100, 81, 64, …, 4, 1)

print(one_through_twenty())
print(evens_to_twenty())
print(odds_to_twenty())
print(multiples_of_five())
print(square_numbers())
print(counting_backwards())
print(even_numbers_backwards())
print(odd_numbers_backwards())
print(multiple_of_five_backwards())
print(square_numbers_backwards())
